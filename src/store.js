import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const API = '/api';

export default new Vuex.Store({
	state: {
		recipes: [],
		recipe: {},
		filters: {
			vegetarian: true,
			non_vegetarian: true,
			people1: true,
			people2: true,
			people4: true,
			people6: true
		},
		filtered: []
	},
	mutations: {
		setRecipes: (state, payload) => {
			state.recipes = payload;
		},
		setRecipe: (state, payload) => {
			state.recipe = payload;
		},
		addRecipe: (state, payload) => {
			state.recipes.unshift(payload);
		},
		removeRecipe: (state, payload) => {
			state.recipes = state.recipes.filter(recipe => recipe._id != payload._id);
		},
		changeRecipe: (state, payload) => {
			state.recipes = state.recipes.map(recipe => {
				return recipe._id == payload._id ? payload : recipe;
			});
		},
		setFilters: (state, payload) => {
			state.filters = payload;
		},
		removeFilters: (state, payload) => {
			state.filters = {
				vegetarian: true,
				non_vegetarian: true,
				people1: true,
				people2: true,
				people4: true,
				people6: true
			};
		}
	},
	actions: {
		getRecipes: ({ commit }) => {
			axios
				.get(`${API}/recipes`)
				.then(response => commit('setRecipes', response.data))
				.catch(err => {
					console.error(err);
				});
		},
		getRecipe: ({ commit }, id) => {
			axios
				.get(`${API}/recipe/${id}`)
				.then(response => commit('setRecipe', response.data))
				.catch(err => {
					console.error(err);
				});
		},
		createRecipe: ({ commit }, recipe) => {
			axios
				.post(`${API}/recipe`, recipe)
				.then(response => commit('addRecipe', response.data))
				.catch(err => {
					console.error(err);
				});
		},
		editRecipe: ({ commit }, recipe) => {
			axios
				.put(`${API}/recipe`, recipe)
				.then(response => commit('changeRecipe', response.data))
				.catch(err => {
					console.error(err);
				});
		},
		deleteRecipe: ({ commit }, id) => {
			axios
				.delete(`${API}/recipe/${id}`)
				.then(response => commit('removeRecipe', response.data))
				.catch(err => {
					console.error(err);
				});
		},
		updateFilters: ({ commit }, filters) => {
			commit('setFilters', filters);
		},
		resetFilters: ({ commit }) => {
			commit('removeFilters');
		}
	},
	getters: {
		recipes: state => {
			return state.recipes.filter(recipe => {
				if (state.filters.vegetarian && recipe.vegetarian) {
					return true;
				} else if (state.filters.non_vegetarian && !recipe.vegetarian) {
					return true;
				} else if (state.filters[`people${recipe.people}`]) {
					return true;
				} else return false;
			});
		},
		recipe: state => state.recipe,
		filters: state => state.filters
	}
});
