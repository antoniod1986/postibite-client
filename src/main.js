import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';
import {
	MdButton,
	MdCard,
	MdIcon,
	MdToolbar,
	MdDialog,
	MdField,
	MdSwitch,
	MdMenu,
	MdList,
	MdChips,
	MdDialogConfirm,
	MdDrawer,
	MdCheckbox
} from 'vue-material/dist/components';

import Vuelidate from 'vuelidate';

Vue.config.productionTip = false;

Vue.use(MdButton);
Vue.use(MdCard);
Vue.use(MdIcon);
Vue.use(MdToolbar);
Vue.use(MdDialog);
Vue.use(Vuelidate);
Vue.use(MdField);
Vue.use(MdSwitch);
Vue.use(MdMenu);
Vue.use(MdList);
Vue.use(MdChips);
Vue.use(MdDialogConfirm);
Vue.use(MdDrawer);
Vue.use(MdCheckbox);

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
