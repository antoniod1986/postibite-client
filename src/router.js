import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import Create from '@/components/Create.vue';
import Edit from '@/components/Edit.vue';
import Recipe from '@/views/Recipe.vue';

Vue.use(Router);

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: Home,
			children: [
				{
					path: 'create',
					component: Create
				},
				{
					path: 'edit/:id',
					component: Edit
				}
			]
		},
		{
			path: '/recipe/:id',
			component: Recipe
		},
		{ path: '*', redirect: '/' }
	]
});
