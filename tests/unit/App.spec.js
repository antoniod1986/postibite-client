import { shallowMount } from '@vue/test-utils';
import App from '@/App.vue';
import router from '@/router';

describe('App.vue', () => {
	it('renders without fail', () => {
		const wrapper = shallowMount(App, {});
		expect(wrapper.exists()).toBe(true);
	});
});
