import { shallowMount } from '@vue/test-utils';
import Home from '@/views/Home.vue';
import router from '@/router';
import Vuex from 'vuex';
import store from '@/store';

describe('Home.vue', () => {
	it('renders without fail', () => {
		const wrapper = shallowMount(Home, {});
		expect(wrapper.exists()).toBe(true);
	});
});
