module.exports = {
	devServer: {
		proxy: {
			'^/api': {
				target: 'http://localhost:4000/v1',
				pathRewrite: { '^/api': '' },
				secure: false,
				logLevel: 'debug',
				changeOrigin: true
			}
		}
	}
};
